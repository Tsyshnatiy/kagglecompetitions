import os
import numpy as np
import cv2
import pandas as pd
from preprocessing import preprocess


def read_train(dir_name, img_shape, labels_filename):
    def count_files(dir_name):
        return len(os.listdir(dir_name))
    
    images_number = count_files(dir_name) - 1
    result = np.empty((images_number, img_shape[0], img_shape[1], img_shape[2]), dtype=float)
    for i in range(images_number):
        img = cv2.imread(dir_name + os.sep + str(i) + '.png')
        result[i, :, :, :] = img
    
    labels = np.load(dir_name + os.sep + labels_filename)
    return result * float(1) / 255, labels


def read_test_images(sample_submission_path, img_path, img_shape, delta):
    sample_submission = pd.read_csv(sample_submission_path)

    images = np.empty((len(sample_submission), img_shape[0], img_shape[1], img_shape[2]), dtype=float)
    test_names = []
    
    for i in range(len(sample_submission)):
        name = sample_submission.iloc[i][0]
        path = img_path + os.sep + str(int(name)) +'.jpg'
        
        img = cv2.imread(path)
        img = preprocess(img, img_shape[0], img_shape[1], delta)
        
        test_names.append(name)
        images[i,:,:,:] = img
    
    return images, test_names


def make_submission(sample_submission_path, test_names, predictions):
    submission = pd.read_csv(sample_submission_path)

    for i, name in enumerate(test_names):
        submission.loc[submission['name'] == name, 'invasive'] = predictions[i]

    submission.to_csv('submit.csv', index=False)
    

def make_predictions(images, models):
    for i in range(len(models)):
        predictions = models[i].predict(images, verbose=1).flatten()
        if i == 0:
            result = predictions
        else:
            result = np.vstack((result, predictions))
    return result.T