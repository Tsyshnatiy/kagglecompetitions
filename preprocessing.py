import cv2

def preprocess(img, target_width, target_height, delta):
    def center_image(img, target_width, target_height, delta):
        result = img[delta:(target_width + delta), delta:(target_height + delta)]
        return result.astype('float32')

    def equalizeHist(img):
        img_yuv = cv2.cvtColor(img, cv2.COLOR_BGR2YUV)

        # equalize the histogram of the Y channel
        img_yuv[:,:,0] = cv2.equalizeHist(img_yuv[:,:,0])

        # convert the YUV image back to RGB format
        return cv2.cvtColor(img_yuv, cv2.COLOR_YUV2BGR)

    result = cv2.resize(img, dsize=(256, 256))
    result = equalizeHist(result)
    result = cv2.cvtColor(result, cv2.COLOR_BGR2RGB)
    result = center_image(result, target_width, target_height, delta)
    
    return result * 1. / 255